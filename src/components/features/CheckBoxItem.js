import React from 'react'

const CheckboxItem = ({ label, isChecked, id, compare }) => (
  <div className='checkbox__item'>
    <label className='checkbox'>
      <div>
        <input className='checkbox__control'
          onChange={() => compare(id)}
          type='checkbox' checked={isChecked} />
        <span className='checkbox__checkmark checkmark'></span>
      </div>
      {label}
    </label>
  </div>
)

export default CheckboxItem