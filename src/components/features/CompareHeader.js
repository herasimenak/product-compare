import React from 'react'
import CheckBoxItem from './CheckBoxItem'

const CompareHeader = ({ products, compare }) => (
  <div className='compare__features--header'>
    <div className='compare__features--title text--blue'>
      Je selectie
    </div>
    {products.map((product, index) => (
      <CheckBoxItem
        key={index}
        label={product.name}
        isChecked={product.compare}
        id={product.id}
        compare={compare}
      />
    ))}
  </div>
)

export default CompareHeader