import React from 'react'
import { extraClassName } from '../../libs/formatting'

const CompareFeatures = ({ compareFeatures }) => (
  <div className='compare__features--list'>
    <span className='compare__features--item'>Keurmerk</span>
    {compareFeatures.map((feature, index) => {
      const className = extraClassName(feature.isDifferent)
      return (
        <span key={index} className={`compare__features--item ${className}`}>{feature.name}</span>
      )
    }
    )}
  </div>
)

export default CompareFeatures