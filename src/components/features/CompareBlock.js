import React from 'react'
import CompareFeatures from './CompareFeatures'
import CompareHeader from './CompareHeader'

const CompareBlock = ({ products, compareFeatures, compare }) => (
  <div className='compare__features--block'>
    <CompareHeader products={products} compare={compare} />
    <CompareFeatures compareFeatures={compareFeatures} />
  </div>
)

export default CompareBlock