import React from 'react'
import CompareBlock from './features/CompareBlock'
import ProductBlock from './products/ProductBlock'

const Compare = ({ products, compareFeatures, compare }) => (
  <div className='compare__container'>
    <CompareBlock
      products={products}
      compare={compare}
      compareFeatures={compareFeatures}
    />
    <ProductBlock
      products={products}
      compareFeatures={compareFeatures}
    />
  </div>
)

export default Compare