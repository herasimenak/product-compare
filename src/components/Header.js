import React from 'react'

const Header = ({ number }) => (
  <div className='header text--blue'>
    <h2>{number} producten vergelijken</h2>
  </div>
)

export default Header