import React from 'react'

const ProductHeader = ({ name, image, price }) => (
  <div className='product'>
    <div className='product__image'>
      <img src={image} alt='image' />
    </div>
    <span className='product__title text--blue'>{name}</span>
    <div className='product__price'>
      <span className='product__price--num'>{price}</span>
      <span className='product__price--unit'>per stuk / excl. btw</span>
    </div>
  </div>
)

export default ProductHeader