import React from 'react'
import ProductBadges from './ProductBadges'
import { extraClassName } from '../../libs/formatting'

const ProductFeatures = ({ product, compareFeatures }) => (
  <div className='compare__features--list'>
    <ProductBadges badges={product.badges} />
    {compareFeatures.map((feature, index) => {
      const className = extraClassName(feature.isDifferent)
      return (
        <span key={index} className={`compare__features--item product--features ${className}`}>
          {product[feature.name]}
        </span>
      )
    })}
  </div>
)

export default ProductFeatures