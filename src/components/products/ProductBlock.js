import React from 'react'
import ProductHeader from './ProductHeader'
import ProductFeatures from './ProductFeatures'

const ProductBlock = ({ products, compareFeatures }) => (
  <div className='compare__products--block'>
    {products.map((product, i) => {
      if (product.compare) {
        return (
          <div className='compare__products--product' key={i}>
            <span className='product--remove'>
              <i className="fas fa-trash-alt"></i>
            </span>
            <ProductHeader name={product.name}
              image={product.productImage}
              price={product.grossPrice}
            />
            <ProductFeatures product={product} compareFeatures={compareFeatures} />
          </div>
        )
      }
      return null
    })}
  </div>
)

export default ProductBlock