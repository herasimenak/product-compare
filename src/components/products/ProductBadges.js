import React from 'react'

const ProductBadges = ({ badges }) => (
  <div className='product__badges'>
    {badges.split('|').map((badge, index) =>
      <img key={index} src={badge} />
    )}
  </div>
)

export default ProductBadges