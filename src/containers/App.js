import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as productActions from '../actions/index'
import Compare from '../components/Compare'
import Header from '../components/Header'

class App extends Component {
  componentDidMount() {
    this.props.actions.getProducts()
  }
  render() {
    const { products, compareFeatures, actions } = this.props
    if (!products && !compareFeatures) {
      return (
        <div>
          <div className='app'>Loading...</div>
        </div>
      )
    }
    return (
      <div>
        <div className='app'>
          <Header number={products.length} />
          <Compare
            compare={actions.compare}
            products={products}
            compareFeatures={compareFeatures}
          />
        </div>
      </div>
    )
  }
}

export default connect(
  state => ({
    products: state.product.products,
    compareFeatures: state.product.compareFeatures
  }),
  dispatch => ({
    actions: bindActionCreators(productActions, dispatch)
  })
)(App)