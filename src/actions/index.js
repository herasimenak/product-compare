import * as types from '../constants/ActionTypes'

const URL = 'https://www.zamro.nl/actions/ViewProduct-ProductCompare?SKU=115E19,11545A,115E1A,115576'
const notCompareFeatures = require('../state/notCompareFeatures.json')

export const getFeatures = () => ({
  type: types.GET_FEATURES,
  payload: notCompareFeatures.notFeatures
})

export const getProducts = () =>
  dispatch =>
    fetch(URL, { method: 'GET' })
      .then(response => response.json())
      .then(response => {
        dispatch({
          type: types.FETCH_PRODUCTS,
          payload: response.products
        })
        dispatch(getFeatures())
      })

export const compare = id => dispatch => {
  return Promise.all([
    dispatch({ type: types.COMPARE_PRODUCT, id }),
    dispatch(getFeatures())
  ])
}

