import * as types from '../constants/ActionTypes'

let InitialState = {
  products: [],
  compareFeatures: []
}

const sortCaseInsensitive = (a, b) => a.name.toLowerCase().localeCompare(b.name.toLowerCase())

const getCompareList = (notIncludesList, products) => {
  let newList = []
  products.map(product => {
    if (product.compare) {
      Object.keys(product).forEach(key => {
        if (!notIncludesList.includes(key) && !newList.includes(key)) {
          return newList.push(key)
        }
        return null
      })
    }
    return null
  })
  let result = newList.map(el => {
    let values = products.map(obj => obj[el])
    return {
      name: el,
      isDifferent: !values.every(v => v===values[0])
    }
  }).sort(sortCaseInsensitive)
  return result
}

export default function (state = InitialState, action) {
  switch (action.type) {
  case types.FETCH_PRODUCTS:
    return {
      ...state, products: action.payload.map((product, index) =>
        ({ ...product, compare: true, id: index })
      )
    }
  case types.GET_FEATURES:
    const newCompareFeatures = getCompareList(action.payload, state.products)
    return {
      ...state, compareFeatures: newCompareFeatures
    }
  case types.COMPARE_PRODUCT:
    return {
      ...state, products: state.products.map(product =>
        product.id === action.id ?
          ({ ...product, compare: !product.compare }) :
          product
      )
    }
  default:
    return state
  }
}